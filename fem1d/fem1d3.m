function x=fem1d3(n)
A=FormMatrix(n);
b=[1; ones(n-1,1)/n; 1];
x=A\b;
end
