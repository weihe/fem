function x=fem1d2(n)
A=FormMatrix(n);
b=[1; zeros(n-1,1); 1];
x=A\b;
end
