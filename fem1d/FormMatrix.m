function A=FormMatrix(n)
A=diag([1 2*n*ones(1,n-1) 1])-n*diag(ones(1,n),-1)-n*diag(ones(1,n),1);
A(1,2)=0;
A(n+1,n)=0;
end
