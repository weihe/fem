function x = fem2d2(n)
A = FormMatrix(n);
b = zeros((n+1)*(n+1),1);
b(1:4*n) = 1;
x = reshape(A\b,n+1,n+1);
end
