function [g, h] = Boundry(n)
x = linspace(0,1,n+1);
y = x;
g = zeros(n,2);
h = zeros(n,2);
g(:,1) = y(1:n) .* y(1:n);
g(:,2) = 1 + y(2:n+1) .* y(2:n+1);
h(:,1) = x(2:n+1) .* x(2:n+1);
h(:,2) = 1 + x(1:n) .* x(1:n);
end
