function A = FormMatrix(n)
A = zeros(n+1,n+1,(n+1)*(n+1));
k = 1;

for j = 1:n
    A(1,j,k) = 1;
    k = k+1;
end
for j = 2:n+1
    A(n+1,j,k) = 1;
    k = k+1;
end
for i = 2:n+1
    A(i,1,k) = 1;
    k = k+1;
end
for i = 1:n
    A(i,n+1,k) = 1;
    k = k+1;
end

w = [-1/3 -1/3 -1/3; -1/3 8/3 -1/3; -1/3 -1/3 -1/3];
for i = 2:n
    for j = 2:n
        A(i-1:i+1,j-1:j+1,k) = w;
        k = k+1;
    end
end

A = reshape(A,(n+1)*(n+1),(n+1)*(n+1))';
end