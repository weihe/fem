n = 128;
[g, h] = Boundry(n);
b = zeros((n+1)*(n+1),1);
b(1:4*n) = [g(:) h(:)];
b(4*n+1:length(b)) = -4 / (n * n);
A = sparse(FormMatrix(n));
x = reshape(A\b,n+1,n+1)